/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.week7;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class UserController {
    private static ArrayList<User> userList = new ArrayList<>();
    private static User curren = null;
    private static User superAdmin = new User("super","super");
    static{
    
        
    }
    //Creat(C)
    public static boolean addUser(User user) throws IOException{
        userList.add(user);
        save();
        return true;
    }
    //Delete (D)
    public static boolean delUser(User user) throws IOException{
        userList.remove(user);
        save();
        return true;
    }
    public static boolean delUser(int index) throws IOException{
        userList.remove(index);
        save();
        return true;
    }
    //Read (R)
    public static ArrayList<User> getUsers(){
        return userList;
    }
    //Update (U)
    public static boolean updateUser(int index,User user) throws IOException{
        userList.set(index, user);
        save();
        return true;
    }
    
    //Flie
    public static void save() throws IOException{
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        
    }
    public static void load(){
         File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    static User getUser(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public  static User login(String userName, String password){
        for(int i =0;i<userList.size();i++){
            User user = userList.get(i);
            if(user.getUserName().equals(userName)&& user.getPassword().equals(password)){
                curren = user;
                return user;
            }
        }
        
        return null;
    }
    
    public static boolean isLogin(){
        return curren != null;
    }
    public static User getCurren(){
        return curren;
    }
    public static void login(){
        curren = null;
    }

    static void logout() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
            
    
}
