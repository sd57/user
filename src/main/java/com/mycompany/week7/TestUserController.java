/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.week7;

/**
 *
 * @author Acer
 */
public class TestUserController {
    public static void main(String[] args) {
        System.out.println(UserController.getUsers());
        //
        UserController.addUser(new User("userx","password"));
        System.out.println(UserController.getUsers());
        //
        User updateUser = new User("usery","password");
        UserController.updateUser(3, updateUser);
        System.out.println(UserController.getUsers());
        //
        UserController.delUser(updateUser);
        System.out.println(UserController.getUsers());
        //
        UserController.delUser(1);
        System.out.println(UserController.getUsers());
    }
}
